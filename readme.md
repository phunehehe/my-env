# my-env

[![pipeline status](https://gitlab.com/phunehehe/my-env/badges/master/pipeline.svg)](https://gitlab.com/phunehehe/my-env/commits/master)

Using [Nix](https://nixos.org/nix/) as a package and configuration management
system outside [NixOS](https://nixos.org/)

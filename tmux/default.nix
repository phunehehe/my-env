{ tmux, writeScriptBin }: writeScriptBin "tmux" ''
  ${tmux}/bin/tmux -f ${./tmux.conf} new-session -ADs default "$@"
''

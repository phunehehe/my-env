#! /usr/bin/env zsh

export TERM=xterm-256color
export MANPAGER='vim -c "%s/_\|.\|\[[0-9]*m//e" -c "set nonumber buftype=nofile filetype=man" -c "goto" -'
export EDITOR=vim
export VISUAL=vim
export PAGER=less

alias fd='fd --hidden'
alias grep='grep --binary-files=without-match --devices=skip --color'
alias less='less --RAW-CONTROL-CHARS'
alias locate='locate --basename --existing'
alias ls='ls --almost-all --classify --human-readable --color=auto -v'
alias ncdu='ncdu -x --color dark'
alias rg="rg --hidden --smart-case --context 2 --glob '!.git'"
#alias rm='~/terminal-dotfiles/garbage-io/garbage-in'

binDirs=(
    "$HOME/go/bin"
    "$HOME/sync/machines/common/bin"
    "$HOME/sync/machines/common/garbage-io/bin"
    /sbin
    /usr/local/bin
    /usr/local/sbin
    /usr/sbin
)
for dir in "${binDirs[@]}"
do
    [[ -d "$dir" ]] && [[ ":$PATH:" =~ .*:$dir:.* ]] || PATH="$dir:$PATH"
done


f=~/.nix-profile/etc/profile.d/nix.sh
[[ -e $f ]] && . $f
f=/usr/local/opt/asdf/libexec/asdf.sh
[[ -e $f ]] && . $f
d=/usr/local/share/zsh/site-functions
[[ -e $d ]] && FPATH=$d:$FPATH
f=/usr/local/bin/direnv
[[ -x $f ]] && eval "$($f hook zsh)"


my_cd() {
    args=("$@")
    if [[ -n $1 && -f $1 ]]
    then
        le_path="$(dirname "$1")"
        echo "$le_path/" 1>&2
        args[1]="$le_path"
    fi
    "cd" "${args[@]}" && ls
}
alias cd=my_cd

purge() {
    target="${1}"
    find "$target" -type f -mtime +7 -delete
    find "$target" -type d -empty -delete
}


wrap() {
    text=$1
    style=$2
    end="$(tput sgr0)"
    echo "%{$style%}$text%{$end%}"
}

code_color="$(tput setab 203)$(tput setaf 16)"
cwd_color="$(tput setaf 38)"
date_color="$(tput setaf 248)"
jobs_color="$(tput setab 221)$(tput setaf 16)"

my_prompt() {

    previous_code=$?

    pieces="$(wrap '%D{%H:%M:%S}' "$date_color")"

    working_dir="$PWD"
    slashes="${working_dir//[^\/]}"
    if ((${#slashes} > 3))
    then
        for _ in $(seq 3 ${#slashes})
        do
            working_dir="${working_dir#*/}"
        done
    fi
    pieces="$pieces $(wrap "$working_dir" "$cwd_color")"

    # `jobs` doesn't work in subshell so dance around a little
    zmodload zsh/parameter
    number_of_jobs=$#jobstates
    ((number_of_jobs > 0)) && pieces="$pieces $(wrap "$number_of_jobs" "$jobs_color")"

    ((previous_code > 0)) && pieces="$pieces $(wrap $previous_code "$code_color")"

    PS1="$pieces "
}
export precmd_functions=(my_prompt)

export HISTFILE=~/.zsh-history
export HISTSIZE=10000
export SAVEHIST=$HISTSIZE
setopt EXTENDED_GLOB
setopt HIST_FCNTL_LOCK
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_LEX_WORDS
setopt HIST_REDUCE_BLANKS
setopt SHARE_HISTORY

zstyle ':completion:*' use-cache on
zstyle ':completion:*' known-hosts-files ''
autoload -U compinit
compinit

bindkey -e
bindkey "[3~"   delete-char
bindkey "OH"    beginning-of-line
bindkey "[H"    beginning-of-line
bindkey "[1~"   beginning-of-line
bindkey "[4~"   end-of-line
bindkey "[F"    end-of-line
bindkey "OF"    end-of-line
bindkey "[1;5C" forward-word
bindkey "[1;5D" backward-word

. "$ZDOTDIR/autojump.zsh"

export ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern line)
. "$ZDOTDIR/zsh-syntax-highlighting.zsh"

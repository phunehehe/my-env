{ autojump
, buildEnv
, coreutils
, runCommand
, writeScriptBin
, zsh
, zsh-syntax-highlighting
}:

let
  zdotdir = runCommand "zdotdir" {} ''
    mkdir $out
    ln --symbolic ${./zshrc} $out/.zshrc
    ln --symbolic ${autojump}/share/autojump/autojump.zsh $out/
    ln --symbolic ${zsh-syntax-highlighting}/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh $out/
  '';

  my-zsh = writeScriptBin "my-zsh" ''
    #! ${zsh}/bin/zsh
    # Do not unset RCS or no history file will be saved
    ZDOTDIR=${zdotdir} exec ${zsh}/bin/zsh --no-global-rcs
  '';

in buildEnv {
  name = "my-zsh-env";
  extraOutputsToInstall = ["man"];
  paths = [
    autojump
    coreutils
    my-zsh
    zsh
  ];
}

(setq-default
 helm-completion-in-region-fuzzy-match t
 helm-echo-input-in-header-line t
 helm-mode-fuzzy-match t
 projectile-use-git-grep t)

(helm-mode 1)
(helm-projectile-on)
(projectile-global-mode)

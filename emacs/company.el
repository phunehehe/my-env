(setq-default
 company-idle-delay 0.1
 company-minimum-prefix-length 2)

(global-company-mode)

(define-key company-active-map (kbd "C-n") 'company-select-next)
(define-key company-active-map (kbd "C-p") 'company-select-previous)

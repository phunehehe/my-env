(setq-default
 evil-flash-delay 10
 evil-shift-width 2
 evil-want-C-u-scroll t)

;; Enable global-evil-leader-mode before evil-mode
;; https://github.com/cofi/evil-leader#usage
(global-evil-leader-mode)
(evil-leader/set-leader "<SPC>")

(global-evil-surround-mode 1)

(evil-mode 1)

(require 'evil-ediff)
(require 'evil-magit)

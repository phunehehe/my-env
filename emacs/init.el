;; https://emacs.stackexchange.com/a/24602/9284
(defun disable-y-or-n-p (orig-fun &rest args)
  (cl-letf (((symbol-function 'y-or-n-p) (lambda (prompt) t)))
    (apply orig-fun args)))

;; http://ergoemacs.org/emacs/elisp_relative_path.html
(defun load-relative (file-name)
  (load (concat (file-name-directory (or load-file-name buffer-file-name))
                file-name)))

(setq-default
 backup-directory-alist `(("." . ,temporary-file-directory))
 compilation-ask-about-save nil
 delete-old-versions t
 ediff-split-window-function 'split-window-horizontally
 ediff-window-setup-function 'ediff-setup-windows-plain
 fill-column 80
 indent-tabs-mode nil
 inhibit-startup-screen t
 require-final-newline t
 revert-without-query '(".*")
 scroll-conservatively 1
 scroll-margin 10
 tab-width 4
 version-control t)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(advice-add 'ediff-quit :around #'disable-y-or-n-p)
(column-number-mode)
(global-auto-revert-mode 1)
(global-linum-mode)
(load-theme 'wombat t)
(menu-bar-mode -1)
(recentf-mode 1)
(show-paren-mode)
(tool-bar-mode -1)

;; https://stackoverflow.com/a/3072831/168034
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tfstate\\'" . javascript-mode))

(load-relative "./company")
(load-relative "./evil")
(load-relative "./helm")

(key-chord-mode 1)
(key-chord-define evil-insert-state-map "jj" 'evil-normal-state)

(evil-leader/set-key
  "bo" 'mode-line-other-buffer
  "bs" 'save-buffer
  "ff" 'helm-find-files
  "fr" 'helm-recentf
  "gc" 'magit-commit
  "gs" 'magit-status
  "pf" 'helm-projectile-find-file
  "pg" 'helm-projectile-grep
  "ps" 'helm-projectile-switch-project
  "q" 'evil-save-modified-and-close
  "wh" 'windmove-left
  "wj" 'windmove-down
  "wk" 'windmove-up
  "wl" 'windmove-right
  "x" 'helm-M-x)

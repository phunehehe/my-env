{ emacsWithPackages, git, runCommand, writeScriptBin }:
let

  emacs = emacsWithPackages (epkgs: with epkgs; [
    company
    evil
    evil-ediff
    evil-leader
    evil-magit
    evil-surround
    helm
    helm-projectile
    key-chord
    magit
    rainbow-delimiters

    clojure-mode
    coffee-mode
    haskell-mode
    markdown-mode
    nix-mode
    php-mode
    terraform-mode
    web-mode
    yaml-mode
  ]);

  bytes = runCommand "bytes" {} ''
    PATH=${git}/bin:$PATH
    cp ${./.}/*.el .
    ${emacs}/bin/emacs --batch --funcall batch-byte-compile *.el
    mkdir --parent $out
    mv *.elc $out/
  '';

in writeScriptBin "my-emacs" ''
  #!/usr/bin/env bash
  exec ${emacs}/bin/emacs --load ${bytes}/init.elc "$@"
''

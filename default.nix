{ pkgs ? import <nixpkgs> {}, ... }:

let

  # Can't use fetchFromGitLab because we want submodules
  brackets = pkgs.fetchgit {
    url = https://gitlab.com/phunehehe/brackets.git;
    rev = "4961f80189458dd2befe336098f6bac25408fbb5";
    sha256 = "0jx1q2cf7cjqh5ms95n98lzyrz1i0qpn75p5rnk4clinrnj9ba6w";
  };

in pkgs.buildEnv {

  name = "my-env";
  extraOutputsToInstall = ["man"];
  ignoreCollisions = true;

  # Add or remove packages from here depending on the machine
  paths = with pkgs; [

    (callPackage ./emacs {})
    (callPackage ./tmux {})
    (callPackage ./vim {})
    (callPackage ./zsh {})
    (callPackage brackets {})
    (haskellPackages.callPackage ./xmonad {})
  ];
}

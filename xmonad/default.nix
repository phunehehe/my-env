{ mkDerivation, base, containers, lib, xmonad, xmonad-contrib }:
mkDerivation {
  pname = "my-xmonad";
  version = "0.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base containers xmonad xmonad-contrib
  ];
  license = lib.licenses.mpl20;
}

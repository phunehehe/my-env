import qualified XMonad                          as X

import           Data.Map                        (Map, fromList, union)
import           XMonad                          ((.|.))
import           XMonad.Actions.PhysicalScreens  (sendToScreen, viewScreen)
import           XMonad.Actions.SwapWorkspaces   (swapWithCurrent)
import           XMonad.Actions.UpdatePointer    (updatePointer)
import           XMonad.Config.Desktop           (desktopConfig)
import           XMonad.Hooks.EwmhDesktops       (ewmhFullscreen)
import           XMonad.Hooks.ManageDocks        (avoidStruts)
import           XMonad.Hooks.SetWMName          (setWMName)
import           XMonad.Layout.Decoration        (Theme)
import           XMonad.Layout.DecorationMadness (tallDwmStyle)
import           XMonad.Layout.Grid              (Grid (GridRatio))
import           XMonad.Layout.LayoutCombinators ((|||))
import           XMonad.Layout.NoBorders         (smartBorders)
import           XMonad.Layout.Tabbed            (decoHeight, fontName, shrinkText, tabbed)
import           XMonad.Prompt                   (XPConfig, font, height)
import           XMonad.Prompt.Shell             (shellPrompt)


myWorkspaces :: [String]
myWorkspaces = fmap show [1..9 :: Integer]

myTheme :: Theme
myTheme = X.def
    { fontName = "xft:NotoSansMono:pixelsize=10"
    , decoHeight = 16
    }

myXPConfig :: XPConfig
myXPConfig = X.def
    { font = "xft:NotoSansMono:pixelsize=40"
    , height = 57
    }


--myLayoutHook :: Layout Window
myLayoutHook = avoidStruts $ smartBorders $
    tabbed shrinkText myTheme
 ||| tallDwmStyle shrinkText myTheme
 ||| GridRatio 1


myKeys :: X.XConfig X.Layout -> [((X.KeyMask, X.KeySym), X.X ())]
myKeys X.XConfig { X.modMask = modm } =
    [((modm, X.xK_p ), shellPrompt myXPConfig)]
    ++
    [((modm .|. X.controlMask, k), X.windows $ swapWithCurrent i)
        | (i, k) <- zip myWorkspaces [X.xK_1..]]
    ++
    [((modm .|. mask, key), f X.def screenIndex)
        | (key, screenIndex) <- zip [X.xK_w, X.xK_e, X.xK_r] [0..]
        , (f, mask) <- [(viewScreen, 0), (sendToScreen, X.shiftMask)]]

newKeys :: X.XConfig X.Layout -> Map (X.ButtonMask, X.KeySym) (X.X ())
newKeys x = fromList (myKeys x) `union` X.keys X.def x


main :: IO ()
main = X.xmonad $ ewmhFullscreen desktopConfig
    { X.layoutHook         = myLayoutHook
    , X.logHook            = X.logHook desktopConfig >> updatePointer (0.8, 0.8) (0, 0)
    , X.terminal           = "wezterm"
    , X.modMask            = X.mod4Mask
    , X.focusedBorderColor = "green"
    , X.borderWidth        = 1
    , X.keys               = newKeys
    , X.workspaces         = myWorkspaces
    , X.startupHook        = setWMName "LG3D"
    }

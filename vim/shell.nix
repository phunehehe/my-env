let
  pkgs = import <nixpkgs> {};

  python = (pkgs.python3Packages.python.withPackages (ps: [
    ps.colormath
  ])).override { ignoreCollisions = true; };

in pkgs.stdenv.mkDerivation rec {
  name = "shell";
  buildInputs = [python];
  phases = ["installPhase"];

  installPhase = ''
    mkdir $out
    ${pkgs.lib.concatMapStringsSep "\n" (i: "ln -s ${i} $out/") buildInputs}
  '';
}

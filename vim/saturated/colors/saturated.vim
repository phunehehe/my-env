let g:colors_name='saturated'

" C_BEST = 0.127 is the highest chroma achievable by all colors at the same lightness
" L_BEST = 0.751 is the corresponding lightness
highlight Normal guifg=#aeaeae guibg=none

" See :help treesitter-highlight-groups
highlight @attribute                  guifg=#ef8bab guibg=none gui=none
highlight @attribute.builtin          guifg=#f18aa3 guibg=none gui=none
highlight @boolean                    guifg=#f28b9c guibg=none gui=none
highlight @character                  guifg=#f48b94 guibg=none gui=none
highlight @comment                    guifg=#f48c8c guibg=none gui=none
highlight @comment.documentation      guifg=#f48d85 guibg=none gui=none
highlight @comment.error              guifg=#f48e7d guibg=none gui=none
highlight @comment.note               guifg=#f38f75 guibg=none gui=none
highlight @comment.todo               guifg=#f2916e guibg=none gui=none
highlight @comment.warning            guifg=#f09367 guibg=none gui=none
highlight @constant                   guifg=#ee9560 guibg=none gui=none
highlight @constant.builtin           guifg=#eb985a guibg=none gui=none
highlight @constant.macro             guifg=#e89a54 guibg=none gui=none
highlight @constructor                guifg=#e49d4f guibg=none gui=none
highlight @diff.delta                 guifg=#e09f4a guibg=none gui=none
highlight @diff.minus                 guifg=#dca246 guibg=none gui=none
highlight @diff.plus                  guifg=#d7a544 guibg=none gui=none
highlight @function                   guifg=#d2a842 guibg=none gui=none
highlight @function.builtin           guifg=#ccab42 guibg=none gui=none
highlight @function.call              guifg=#c6ae44 guibg=none gui=none
highlight @function.macro             guifg=#bfb046 guibg=none gui=none
highlight @function.method            guifg=#b8b34a guibg=none gui=none
highlight @function.method.call       guifg=#b1b64e guibg=none gui=none
highlight @keyword                    guifg=#a9b854 guibg=none gui=none
highlight @keyword.conditional        guifg=#a1bb5a guibg=none gui=none
highlight @keyword.coroutine          guifg=#99bd60 guibg=none gui=none
highlight @keyword.debug              guifg=#90bf67 guibg=none gui=none
highlight @keyword.directive          guifg=#87c16f guibg=none gui=none
highlight @keyword.directive.define   guifg=#7ec376 guibg=none gui=none
highlight @keyword.exception          guifg=#74c47e guibg=none gui=none
highlight @keyword.function           guifg=#6ac586 guibg=none gui=none
highlight @keyword.import             guifg=#60c68e guibg=none gui=none
highlight @keyword.modifier           guifg=#55c796 guibg=none gui=none
highlight @keyword.operator           guifg=#4bc89e guibg=none gui=none
highlight @keyword.repeat             guifg=#3fc8a6 guibg=none gui=none
highlight @keyword.return             guifg=#34c8ad guibg=none gui=none
highlight @keyword.type               guifg=#28c8b5 guibg=none gui=none
highlight @label                      guifg=#1cc8bd guibg=none gui=none
highlight @markup.heading             guifg=#10c7c4 guibg=none gui=none
highlight @markup.link                guifg=#08c6cb guibg=none gui=none
highlight @markup.link.label          guifg=#09c5d1 guibg=none gui=none
highlight @markup.list                guifg=#12c4d8 guibg=none gui=none
highlight @markup.list.checked        guifg=#1ec3de guibg=none gui=none
highlight @markup.list.unchecked      guifg=#2ac1e3 guibg=none gui=none
highlight @markup.math                guifg=#35bfe9 guibg=none gui=none
highlight @markup.quote               guifg=#40bded guibg=none gui=none
highlight @markup.raw                 guifg=#4bbbf1 guibg=none gui=none
highlight @markup.raw.block           guifg=#55b9f5 guibg=none gui=none
highlight @module                     guifg=#5fb7f8 guibg=none gui=none
highlight @module.builtin             guifg=#69b4fb guibg=none gui=none
highlight @number                     guifg=#73b2fd guibg=none gui=none
highlight @number.float               guifg=#7caffe guibg=none gui=none
highlight @property                   guifg=#84adff guibg=none gui=none
highlight @punctuation.special        guifg=#8daaff guibg=none gui=none
highlight @string                     guifg=#95a8fe guibg=none gui=none
highlight @string.documentation       guifg=#9da5fd guibg=none gui=none
highlight @string.escape              guifg=#a5a3fc guibg=none gui=none
highlight @string.regexp              guifg=#aca0f9 guibg=none gui=none
highlight @string.special             guifg=#b39ef7 guibg=none gui=none
highlight @string.special.path        guifg=#ba9bf3 guibg=none gui=none
highlight @string.special.symbol      guifg=#c099ef guibg=none gui=none
highlight @tag                        guifg=#c697eb guibg=none gui=none
highlight @tag.attribute              guifg=#cc95e6 guibg=none gui=none
highlight @tag.builtin                guifg=#d193e1 guibg=none gui=none
highlight @type                       guifg=#d691db guibg=none gui=none
highlight @type.builtin               guifg=#db90d5 guibg=none gui=none
highlight @type.definition            guifg=#df8fcf guibg=none gui=none
highlight @variable.builtin           guifg=#e38dc8 guibg=none gui=none
highlight @variable.member            guifg=#e78cc1 guibg=none gui=none
highlight @variable.parameter         guifg=#ea8bba guibg=none gui=none
highlight @variable.parameter.builtin guifg=#ed8bb2 guibg=none gui=none

" C = 0.153 is the highest chroma that can give 4 colors at the same lightness
" L = 0.755 is the corresponding lightness
highlight RainbowDelimiterCyan   guifg=#08bfff guibg=none gui=none
highlight RainbowDelimiterViolet guifg=#dc8ce9 guibg=none gui=none
highlight RainbowDelimiterOrange guifg=#fa904a guibg=none gui=none
highlight RainbowDelimiterGreen  guifg=#72c76a guibg=none gui=none

" C = C_BEST * 3 / 4 = 0.0953
" L = 0.284 is the lowest lightness that can give 3 colors at C
highlight DiffText   guifg=#aeaeae guibg=#0d2658

" C = C_BEST / 2 = 0.0635
highlight DiffAdd    guifg=#aeaeae guibg=#153113
highlight DiffChange guifg=#aeaeae guibg=#2a2a2a
highlight DiffDelete guifg=#441b1b guibg=#441b1b

highlight link @markup.italic        Normal
highlight link @markup.link.url      @markup.underline
highlight link @markup.strikethrough Normal
highlight link @markup.strong        Normal
highlight link @markup.underline     Normal
highlight link @string.special.url   @markup.underline

highlight! link @character.special           Normal
highlight! link @keyword.conditional.ternary Normal
highlight! link @operator                    Normal
highlight! link @punctuation.bracket         Normal
highlight! link @punctuation.delimiter       Normal
highlight! link @tag.delimiter               Normal
highlight! link @variable                    Normal

highlight! link @markup.heading.1 @markup.heading
highlight! link @markup.heading.2 @markup.heading
highlight! link @markup.heading.3 @markup.heading
highlight! link @markup.heading.4 @markup.heading
highlight! link @markup.heading.5 @markup.heading
highlight! link @markup.heading.6 @markup.heading

" See :help group-name
highlight! link Added          @diff.plus
highlight! link Boolean        @boolean
highlight! link Changed        @diff.delta
highlight! link Character      @character
highlight! link Comment        @comment
highlight! link Conditional    @keyword.conditional
highlight! link Constant       @constant
highlight! link Debug          @keyword.debug
highlight! link Define         @keyword.directive.define
highlight! link Delimiter      @punctuation.delimiter
highlight! link Error          @comment.error
highlight! link Exception      @keyword.exception
highlight! link Float          @number.float
highlight! link Function       @function
highlight! link Identifier     @variable
highlight! link Include        @keyword.import
highlight! link Keyword        @keyword
highlight! link Label          @label
highlight! link Macro          @function.macro
highlight! link Number         @number
highlight! link Operator       @operator
highlight! link PreCondit      @keyword.conditional
highlight! link PreProc        @keyword.directive
highlight! link Removed        @diff.minus
highlight! link Repeat         @keyword.repeat
highlight! link Special        @punctuation.special
highlight! link SpecialChar    @character.special
highlight! link SpecialComment @comment.note
highlight! link Statement      @keyword
highlight! link StorageClass   @keyword.modifier
highlight! link String         @string
highlight! link Structure      @keyword.type
highlight! link Tag            @tag
highlight! link Todo           @comment.todo
highlight! link Type           @type
highlight! link Typedef        @type.definition
highlight! link Underlined     @markup.underline

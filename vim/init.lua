vim.g.mapleader = ' '

require 'nvim-surround'.setup {}
require 'nvim-treesitter.configs'.setup { highlight = { enable = true } }
vim.keymap.set('n', 'f', '<Plug>(leap)')


require 'neo-tree'.setup {
  default_component_configs = {
    git_status = { symbols = false },
    icon = { enabled = false },
  },
  window = { mappings = { ['v'] = 'open_vsplit' } },
}

vim.keymap.set('n', '<Leader>n', '<cmd>Neotree reveal<cr>')


require('rainbow-delimiters.setup').setup {
  highlight = {
    'RainbowDelimiterCyan',
    'RainbowDelimiterViolet',
    'RainbowDelimiterOrange',
    'RainbowDelimiterGreen',
  },
}


require('telescope').setup {
  defaults = {
    mappings = {
      n = {
        ['v'] = 'select_vertical',
        ['t'] = 'select_tab',
      }
    }
  }
}

vim.keymap.set('n', '<Leader>ff', ':Telescope find_files<cr>')
vim.keymap.set('n', '<Leader>fg', ':Telescope live_grep<cr>')
vim.keymap.set('n', '<Leader>fr', ':Telescope oldfiles<cr>')


-- Completion
vim.keymap.set('i', '<c-j>', '<c-n>')
vim.keymap.set('i', '<c-k>', '<c-p>')
vim.g['deoplete#enable_at_startup'] = 1
vim.fn['deoplete#custom#option']('min_pattern_length', 1)
vim.fn['deoplete#custom#option']('max_abbr_width', 99)
vim.fn['deoplete#custom#option']('max_menu_width', 99)

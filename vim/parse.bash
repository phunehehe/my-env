#! /usr/bin/env bash
set -efuxo pipefail

f() {
  find sample -name "*.$1" \
  | shuf \
  | while read -r f
    do
      sed '/^[[:space:]]*$/d' "$f" >> "sample.$1"
      words=$(wc --words < "sample.$1")
      [[ $words -gt 9999 ]] && break
    done \
  || true

  vim \
    '+let g:html_number_lines = 0' \
    '+let g:html_no_progress = 1' \
    +TOhtml \
    "+w sample.$1.html" \
    +qa \
    "sample.$1"
}

f ex
f md
f rb
f rs
f sql

groups=(
  -attribute
  -attribute-builtin
  -boolean
  -character
  -character-special
  -comment
  -comment-documentation
  -comment-error
  -comment-note
  -comment-todo
  -comment-warning
  -constant
  -constant-builtin
  -constant-macro
  -constructor
  -diff-delta
  -diff-minus
  -diff-plus
  -function
  -function-builtin
  -function-call
  -function-macro
  -function-method
  -function-method-call
  -keyword
  -keyword-conditional
  -keyword-conditional-ternary
  -keyword-coroutine
  -keyword-debug
  -keyword-directive
  -keyword-directive-define
  -keyword-exception
  -keyword-function
  -keyword-import
  -keyword-modifier
  -keyword-operator
  -keyword-repeat
  -keyword-return
  -keyword-type
  -label
  -markup-heading
  -markup-heading-1
  -markup-heading-2
  -markup-heading-3
  -markup-heading-4
  -markup-heading-5
  -markup-heading-6
  -markup-italic
  -markup-link
  -markup-link-label
  -markup-link-url
  -markup-list
  -markup-list-checked
  -markup-list-unchecked
  -markup-math
  -markup-quote
  -markup-raw
  -markup-raw-block
  -markup-strikethrough
  -markup-strong
  -markup-underline
  -module
  -module-builtin
  -number
  -number-float
  -operator
  -property
  -punctuation-bracket
  -punctuation-delimiter
  -punctuation-special
  -string
  -string-documentation
  -string-escape
  -string-regexp
  -string-special
  -string-special-path
  -string-special-symbol
  -string-special-url
  -tag
  -tag-attribute
  -tag-builtin
  -tag-delimiter
  -type
  -type-builtin
  -type-definition
  -variable
  -variable-builtin
  -variable-member
  -variable-parameter
  -variable-parameter-builtin
)

pattern=$(IFS='|'; echo "${groups[*]}")
pattern='class="('$pattern')"'

find . -name 'sample.*.html' -print0 \
| xargs -0 grep --extended-regexp --no-filename --only-matching "$pattern" \
| cut --delimiter '"' --field 2 \
| sort \
| uniq --count \
| sort --numeric --reverse \
| awk '{printf "'\''%s'\'',\n", $2}'

{ neovim-unwrapped, vimPlugins, vimUtils, wrapNeovimUnstable }:
let
  saturated = vimUtils.buildVimPlugin {
    name = "saturated";
    src = ./saturated;
  };

in (wrapNeovimUnstable neovim-unwrapped) {
  luaRcContent = builtins.readFile ./init.lua;
  neovimRcContent = builtins.readFile ./vimrc;

  plugins = with vimPlugins; [
    deoplete-nvim
    fugitive
    leap-nvim
    neo-tree-nvim
    nvim-surround
    nvim-treesitter.withAllGrammars
    rainbow-delimiters-nvim
    saturated
    telescope-nvim
    undotree
  ];
}
